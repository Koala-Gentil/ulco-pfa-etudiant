import Data.Array
import Text.Read (readMaybe)

type Tab = Array Int Double

newTab :: Tab
newTab = array (1,4) [(1,0.0),(2,0.0),(3,0.0),(4,0.0)]

inBounds :: Tab -> Int -> Bool
inBounds tab i = let (b0, b1) = bounds tab in i >= b0 && i <= b1

loopMaj :: Tab -> IO Tab
loopMaj tab = do
    print $ tab
    input <- getLine
    case readMaybe input :: Maybe (Int, Double) of
        Just (i, v) -> if inBounds tab i 
            then loopMaj (tab // [(i, v)])
            else return tab
        Nothing -> return tab

main :: IO ()
main = do
    tab <- loopMaj newTab
    print tab
