import Control.Monad (forM_)
import System.IO (hFlush, stdout)
import Text.Read (readMaybe)

readInt :: Int -> IO ()
readInt i = do
    putStr $ "saisie " ++ show i ++ " : "
    input <- getLine
    case readMaybe input :: Maybe Int of 
        Just x  -> putStrLn $ "Vous avez saisi l'entier " ++ show x
        Nothing -> putStrLn "saisie invalide"

main :: IO ()
main = forM_ [1..4] readInt

