intToFizzbuz :: Int -> String
intToFizzbuz x
    | x `mod` 15 == 0 = "fizzbuzz"
    | x `mod` 5 == 0 = "buzz"
    | x `mod` 3 == 0 = "fizz"
    | otherwise = show x

fizzbuzz1 :: [Int] -> [String]
fizzbuzz1 [] = []
fizzbuzz1 (x:xs) = intToFizzbuz x : fizzbuzz1 xs

fizzbuzz2 :: [Int] -> [String]
fizzbuzz2 xs = reverse $ aux xs []
    where aux [] acc = acc
          aux (x:xs) acc = aux xs (intToFizzbuz x : acc)

fizzbuzz :: [String]
fizzbuzz = [intToFizzbuz x | x <- [1..]]

main :: IO ()
main = putStrLn "TODO"

