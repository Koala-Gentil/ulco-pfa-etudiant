seuilInt :: Int -> Int -> Int -> Int
seuilInt min max v
    | v < min = min
    | v > max = max
    | otherwise = v

seuilTuple :: (Int, Int) -> Int -> Int
seuilTuple (min, max) v
    | v < min = min
    | v > max = max
    | otherwise = v

main :: IO ()
main = do
    print $ seuilInt 3 4 5
    print $ seuilTuple (3, 4) 5
